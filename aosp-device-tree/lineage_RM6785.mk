#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from RM6785 device
$(call inherit-product, device/realme/RM6785/device.mk)

PRODUCT_DEVICE := RM6785
PRODUCT_NAME := lineage_RM6785
PRODUCT_BRAND := realme
PRODUCT_MODEL := RM6785
PRODUCT_MANUFACTURER := realme

PRODUCT_GMS_CLIENTID_BASE := android-oppo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="evolution_RM6785-userdebug 14 UD1A.230803.041 1697808208 release-keys"

BUILD_FINGERPRINT := realme/RMX2001/RMX2001L1:10/QP1A.190711.020/1594211000:user/release-keys
