#!/system/bin/sh
GPU_MAX_FREQ=$(cat /proc/gpufreq/max_freq)
## Common-default settings

# GED modules
echo 0 >/sys/module/ged/parameters/gx_game_mode
echo 0 >/sys/module/ged/parameters/gx_force_cpu_boost
echo 0 > /sys/module/ged/parameters/boost_amp
echo 0 > /sys/module/ged/parameters/boost_extra
echo 0 > /sys/module/ged/parameters/boost_gpu_enable
echo 1 > /sys/module/ged/parameters/enable_cpu_boost
echo 0 > /sys/module/ged/parameters/enable_gpu_boost
echo 0 > /sys/module/ged/parameters/enable_game_self_frc_detect
echo 30 > /sys/module/ged/parameters/cpu_boost_policy
echo 80 > /sys/module/ged/parameters/gpu_idle
echo 0 > /sys/module/ged/parameters/ged_boost_enable
echo 0 > /sys/module/ged/parameters/ged_smart_boost
echo 0 > /sys/module/ged/parameters/gx_frc_mode

# GPU Power Policy
 echo GPU Power Policy
 echo 0 > /proc/mali/always_on
 cat /proc/mali/always_on
 echo coarse_demand > /sys/devices/platform/13040000.mali/power_policy
 cat /sys/devices/platform/13040000.mali/power_policy
 echo

#GPUSchedMode
#switch to system priority Mode
echo 0 >/sys/devices/platform/13040000.mali/js_ctx_scheduling_mode


# Enable PPM
echo 1 > /proc/ppm/enabled

# CPU Stune
    echo Modify CPUStune
    
    # CPU Load settings
    echo 0-7 > /dev/cpuset/foreground/cpus
    echo 0-5 > /dev/cpuset/background/cpus
    echo 0-5 > /dev/cpuset/system-background/cpus
    echo 0-7 > /dev/cpuset/top-app/cpus
    echo 0 > /dev/cpuset/restricted/cpus
    
    # Realtime
    echo 8 > /dev/stune/rt/schedtune.boost
    echo 1 > /dev/stune/rt/schedtune.prefer_idle
    
    # Background
    echo 0 > /dev/stune/background/schedtune.util.max.effective
    echo 0 > /dev/stune/background/schedtune.util.min.effective
    echo 865 > /dev/stune/background/schedtune.util.max
    echo 0 > /dev/stune/background/schedtune.util.min
    echo 0 > /dev/stune/background/schedtune.boost
    echo 0 > /dev/stune/background/schedtune.prefer_idle
    
    # Foreground
    echo 865 > /dev/stune/foreground/schedtune.util.max.effective
    echo 0 > /dev/stune/foreground/schedtune.util.min.effective
    echo 865 > /dev/stune/foreground/schedtune.util.max
    echo 0 > /dev/stune/foreground/schedtune.util.min
    echo 0 > /dev/stune/foreground/schedtune.boost
    echo 0 > /dev/stune/foreground/schedtune.prefer_idle
    
    # Top-App
    echo 865> /dev/stune/top-app/schedtune.util.max.effective
    echo 0 > /dev/stune/top-app/schedtune.util.min.effective
    echo 865 > /dev/stune/top-app/schedtune.util.max
    echo 0 > /dev/stune/top-app/schedtune.util.min
    echo 0 > /dev/stune/top-app/schedtune.boost
    echo 0 > /dev/stune/top-app/schedtune.prefer_idle
    
    # Global
    echo 0 > /dev/stune/schedtune.util.min
    echo 865 > /dev/stune/schedtune.util.max
    echo 865 > /dev/stune/schedtune.util.max.effective
    echo 0 > /dev/stune/schedtune.util.min.effective
    echo 8 > /dev/stune/schedtune.boost
    echo 0 > /dev/stune/schedtune.prefer_idle
    echo



 echo 0 1 > /proc/ppm/policy_status
    echo 1 0 > /proc/ppm/policy_status
    echo 2 0 > /proc/ppm/policy_status
    echo 3 1 > /proc/ppm/policy_status
    echo 4 1 > /proc/ppm/policy_status
    echo 5 1 > /proc/ppm/policy_status
    echo 6 0 > /proc/ppm/policy_status
    echo 7 0 > /proc/ppm/policy_status
    echo 8 1 > /proc/ppm/policy_status
    echo 9 1 > /proc/ppm/policy_status	

	echo Balanced Mode Enabled

# Enable Thermal Throttle
echo 4 1 > /proc/ppm/policy_status

# cpufreq mode
echo 0 > /proc/cpufreq/cpufreq_cci_mode
echo 0 > /proc/cpufreq/cpufreq_power_mode
echo 0 > /sys/devices/system/cpu/perf/enable

# Default CPU Freq
#big cluster
echo 0 -1 > /proc/ppm/policy/hard_userlimit_max_cpu_freq
echo 0 -1 > /proc/ppm/policy/hard_userlimit_min_cpu_freq
#LITTLE cluster
echo 1 -1 > /proc/ppm/policy/hard_userlimit_max_cpu_freq
echo 1 -1 > /proc/ppm/policy/hard_userlimit_min_cpu_freq

# Default GPU Freq
echo 0 > /proc/gpufreq/gpufreq_opp_freq

# set sched to hybrid (HMP,EAS)
echo 2 > /sys/devices/system/cpu/eas/enable

# schedutil rate-limit
echo 7000 > /sys/devices/system/cpu/cpufreq/schedutil/down_rate_limit_us
echo 10000 > /sys/devices/system/cpu/cpufreq/schedutil/up_rate_limit_us

# cpusets
echo 0-7 > /dev/cpuset/foreground/cpus
echo 0-5 > /dev/cpuset/background/cpus
echo 0-7 > /dev/cpuset/system-background/cpus
echo 0-7 > /dev/cpuset/top-app/cpus
echo 0-7 > /dev/cpuset/restricted/cpus

# OPPO touchpanel
echo 0 > /proc/touchpanel/oppo_tp_limit_enable
echo 1 > /proc/touchpanel/oppo_tp_direction
echo 0 > /proc/touchpanel/game_switch_enable




    echo Change Governor to schedutil
    echo schedutil > /sys/devices/system/cpu/cpufreq/policy0/scaling_governor
    echo schedutil > /sys/devices/system/cpu/cpufreq/policy6/scaling_governor

case $1 in
0)
	## Balanced profile(use all default settings)
	;;
1)
	## Power saving profile

    # GED Modules
    echo 0 >/sys/module/ged/parameters/gx_game_mode
	echo 0 >/sys/module/ged/parameters/gx_force_cpu_boost
	echo 0 > /sys/module/ged/parameters/boost_amp
	echo 0 > /sys/module/ged/parameters/boost_extra
	echo 0 > /sys/module/ged/parameters/boost_gpu_enable
	echo 0 > /sys/module/ged/parameters/enable_cpu_boost
        echo 10 > /sys/module/ged/parameters/cpu_boost_policy
	echo 0 > /sys/module/ged/parameters/enable_gpu_boost
	echo 0 > /sys/module/ged/parameters/enable_game_self_frc_detect
	echo 100 > /sys/module/ged/parameters/gpu_idle

# GPU Power Policy
 echo GPU Power Policy
 echo 0 > /proc/mali/always_on
 cat /proc/mali/always_on
 echo coarse_demand > /sys/devices/platform/13040000.mali/power_policy
 cat /sys/devices/platform/13040000.mali/power_policy
 echo

     #RAM Optimizations
      echo "100"> /proc/sys/vm/vfs_cache_pressure
       echo "19"> /proc/sys/vm/stat_interval
        echo "20"> /proc/sys/vm/dirty_ratio
        echo "8"> /proc/sys/vm/dirty_background_ratio
        echo "160"> /proc/sys/vm/direct_swapiness
echo "120"> /proc/sys/vm/swapiness
	
	# cpufreq mode
	echo 1 >/proc/cpufreq/cpufreq_power_mode
	echo 0 >/proc/cpufreq/cpufreq_cci_mode
	echo 0 > /sys/devices/system/cpu/perf/enable

    # force sched to EAS
    echo 1 > /sys/devices/system/cpu/eas/enable

    # sched rate
	echo 168 > /sys/devices/system/cpu/cpufreq/schedutil/down_rate_limit_us
	echo 1000 > /sys/devices/system/cpu/cpufreq/schedutil/up_rate_limit_us
	
#GPUSchedMode
#switch to system priority Mode
echo 0 >/sys/devices/platform/13040000.mali/js_ctx_scheduling_mode

	# CPU SchedTune
	echo 0-5 > /dev/cpuset/foreground/cpus
	echo 0-3 > /dev/cpuset/background/cpus
	echo 0-7 > /dev/cpuset/system-background/cpus
	echo 0-7 > /dev/cpuset/top-app/cpus
	echo 0-4 > /dev/cpuset/restricted/cpus
	
	# PPM
	echo 1 > /proc/ppm/enabled
	echo 0 0 > /proc/ppm/policy_status
	echo 1 0 > /proc/ppm/policy_status
	echo 2 1 > /proc/ppm/policy_status
	echo 3 1 > /proc/ppm/policy_status
	echo 5 0 > /proc/ppm/policy_status
	echo 6 1 > /proc/ppm/policy_status
	echo 7 1 > /proc/ppm/policy_status
	echo 8 1 > /proc/ppm/policy_status
	echo 9 0 > /proc/ppm/policy_status

	#enable thermal throttle
    echo 4 1 > /proc/ppm/policy_status

 
    # LITTLE cluster
    echo 0 1618000 >/proc/ppm/policy/hard_userlimit_max_cpu_freq
    echo 0 774000 >/proc/ppm/policy/hard_userlimit_min_cpu_freq
    echo Done
    echo

    # limit minimum cpu frequency
    # big cluster
    echo 1 1419000 >/proc/ppm/policy/hard_userlimit_max_cpu_freq
    echo 1 774000 >/proc/ppm/policy/hard_userlimit_min_cpu_freq
    echo Done
    echo
    
    # LITTLE cluster
    echo 0 1618000 >/proc/ppm/policy/hard_userlimit_max_cpu_freq
    echo 0 774000 >/proc/ppm/policy/hard_userlimit_min_cpu_freq
    echo Done
    echo

# schedutil rate-limit
echo 1000 > /sys/devices/system/cpu/cpufreq/schedutil/down_rate_limit_us
echo 2800 > /sys/devices/system/cpu/cpufreq/schedutil/up_rate_limit_us

# set sched to EAS
	echo Change kernel mode to EAS Mode
	echo 1 > /sys/devices/system/cpu/eas/enable
	echo Done
	echo

    # Scheduler I/O
    echo Scheduler I/O 
    echo cfq > /sys/block/mmcblk0/queue/scheduler
    cat /sys/block/mmcblk0/queue/scheduler

# schedutil rate-limit
echo 1000 > /sys/devices/system/cpu/cpufreq/schedutil/down_rate_limit_us
echo 2800 > /sys/devices/system/cpu/cpufreq/schedutil/up_rate_limit_us

# set sched to EAS
	echo Change kernel mode to EAS Mode
	echo 1 > /sys/devices/system/cpu/eas/enable
	echo Done
	echo

    # Scheduler I/O
    echo Scheduler I/O 
    echo cfq > /sys/block/mmcblk0/queue/scheduler
    cat /sys/block/mmcblk0/queue/scheduler
	
	# Lock Lowest GPU Freq
    echo 270000 > /proc/gpufreq/gpufreq_opp_freq
	
	# OPPO touchpanel
	echo 0 > /proc/touchpanel/oplus_tp_limit_enable
    echo 1 > /proc/touchpanel/oplus_tp_direction
    echo 0 > /proc/touchpanel/game_switch_enable
	;;
2)
	## Performance Profile

   # POWERHAL SPORT MODE
	echo Add some games to sport mode
	echo -e "com.mobile.legends\ncom.tencent.ig\nskynet.cputhrottlingtest\ncom.miHoYo.GenshinImpact\ncom.pubg.krmobile\ncom.tencent.tmgp.pubgmhd\ncom.dts.freefireth\ncom.dts.freefiremax\njp.konami.pesam\ncom.pubg.newstate\ncom.garena.game.codm\ncom.pubg.imobile\ncom.ea.gp.apexlegendsmobilefps\ncom.riotgames.league.wildrift\ncom.antutu.ABenchMark\ncom.primatelabs.geekbench" > /data/vendor/powerhal/smart
	echo Done
   
#RAM Optimizations
echo "100"> /proc/sys/vm/vfs_cache_pressure
echo "10"> /proc/sys/vm/stat_interval
echo "9"> /proc/sys/vm/dirty_ratio
echo "3"> /proc/sys/vm/dirty_background_ratio
echo "160"> /proc/sys/vm/direct_swapiness
echo "110"> /proc/sys/vm/swappiness

    # GED Modules
	echo 1 >/sys/module/ged/parameters/gx_game_mode
	echo 1 >/sys/module/ged/parameters/gx_force_cpu_boost
	echo 1 > /sys/module/ged/parameters/boost_amp
	echo 1 > /sys/module/ged/parameters/boost_extra
	echo 1 > /sys/module/ged/parameters/boost_gpu_enable
	echo 1 > /sys/module/ged/parameters/enable_cpu_boost
        echo 99 > /sys/module/ged/parameters/cpu_boost_policy
	echo 1 > /sys/module/ged/parameters/enable_gpu_boost
	echo 1 > /sys/module/ged/parameters/enable_game_self_frc_detect
	echo 0 > /sys/module/ged/parameters/gpu_idle

    # cpufreq mode
	echo 3 > /proc/cpufreq/cpufreq_power_mode
	echo 1 > /proc/cpufreq/cpufreq_cci_mode
    echo 1 > /sys/devices/system/cpu/perf/enable

	# force sched to HMP
	echo 0 > /sys/devices/system/cpu/eas/enable

    # sched rate
	echo 0 > /sys/devices/system/cpu/cpufreq/schedutil/down_rate_limit_us
	echo 0 > /sys/devices/system/cpu/cpufreq/schedutil/up_rate_limit_us

    # CPU SchedTune
	echo 0-7 > /dev/cpuset/foreground/cpus
	echo 4-7 > /dev/cpuset/background/cpus
	echo 0-7 > /dev/cpuset/system-background/cpus
	echo 0-7 > /dev/cpuset/top-app/cpus
	echo 0 > /dev/cpuset/restricted/cpus

    # PPM
	echo 1 > /proc/ppm/enabled
	echo 0 1 > /proc/ppm/policy_status
	echo 1 1 > /proc/ppm/policy_status
	echo 2 1 > /proc/ppm/policy_status
	echo 3 1 > /proc/ppm/policy_status
	echo 5 1 > /proc/ppm/policy_status
	echo 6 1 > /proc/ppm/policy_status
	echo 7 1 > /proc/ppm/policy_status
	echo 8 1 > /proc/ppm/policy_status
	echo 9 1 > /proc/ppm/policy_status

	#disable thermal throttle
    echo 4 0 > /proc/ppm/policy_status

# GPU Power Policy
 echo GPU Power Policy
 echo 1 > /proc/mali/always_on
 cat /proc/mali/always_on
 echo always_on > /sys/devices/platform/13040000.mali/power_policy
 cat /sys/devices/platform/13040000.mali/power_policy
 echo



    # Set maximum CPU Freq
    # big cluster
	echo 1 2050000 >/proc/ppm/policy/hard_userlimit_max_cpu_freq
	echo 1 2050000 >/proc/ppm/policy/hard_userlimit_min_cpu_freq
    # LITTLE cluster
	echo 0 2000000 >/proc/ppm/policy/hard_userlimit_max_cpu_freq
	echo 0 2000000 >/proc/ppm/policy/hard_userlimit_min_cpu_freq
	
    # Set maximum GPU Freq
    # GPU frequency
     echo GPU Frequency
        if [ "$GPU_MAX_FREQ" == "900000" ]; then
   echo 900000 > /proc/gpufreq/gpufreq_opp_freq
    else
   echo 806000 > /proc/gpufreq/gpufreq_opp_freq
        fi

#GPUSchedMode
#switch to process priority Mode
echo 1 >/sys/devices/platform/13040000.mali/js_ctx_scheduling_mode

    # OPPO touchpanel
	echo 0 > /proc/touchpanel/oplus_tp_limit_enable
	echo 1 > /proc/touchpanel/oplus_tp_direction
	echo 0 > /proc/touchpanel/game_switch_enable


	# CPUStune
	echo Modify CPUStune
	
	# CPU Load settings
	echo 0-7 > /dev/cpuset/foreground/cpus
	echo 0-5 > /dev/cpuset/background/cpus
	echo 0-3 > /dev/cpuset/system-background/cpus
	echo 0-7 > /dev/cpuset/top-app/cpus
	echo 0 > /dev/cpuset/restricted/cpus
	
	# Realtime
	echo 1 > /dev/stune/rt/schedtune.boost
	echo 1 > /dev/stune/rt/schedtune.prefer_idle
	
	# Background
	echo 0 > /dev/stune/background/schedtune.util.max.effective
	echo 0 > /dev/stune/background/schedtune.util.min.effective
	echo 0 > /dev/stune/background/schedtune.util.max
	echo 0 > /dev/stune/background/schedtune.util.min
	echo 0 > /dev/stune/background/schedtune.boost
	echo 1 > /dev/stune/background/schedtune.prefer_idle
	
	# Foreground
	echo 1024 > /dev/stune/foreground/schedtune.util.max.effective
	echo 0 > /dev/stune/foreground/schedtune.util.min.effective
	echo 1024 > /dev/stune/foreground/schedtune.util.max
	echo 0 > /dev/stune/foreground/schedtune.util.min
	echo 0 > /dev/stune/foreground/schedtune.boost
	echo 0> /dev/stune/foreground/schedtune.prefer_idle
	
	# Top-App
	echo 1024 > /dev/stune/top-app/schedtune.util.max.effective
	echo 1024 > /dev/stune/top-app/schedtune.util.max
	echo 1 > /dev/stune/top-app/schedtune.boost
	echo 0 > /dev/stune/top-app/schedtune.prefer_idle
	
	# Global
	echo 0 > /dev/stune/schedtune.util.min
	echo 1024 > /dev/stune/schedtune.util.max
	echo 1024 > /dev/stune/schedtune.util.max.effective
	echo 0 > /dev/stune/schedtune.util.min.effective
	echo 0 > /dev/stune/schedtune.boost
	echo 0 > /dev/stune/schedtune.prefer_idle
	echo




    echo Change Governor to perf
    echo performance > /sys/devices/system/cpu/cpufreq/policy0/scaling_governor
    echo performance > /sys/devices/system/cpu/cpufreq/policy6/scaling_governor

    	# increase performance
	echo 1 > /sys/devices/system/cpu/perf/enable
    

	;;
esac
